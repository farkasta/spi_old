(function() {
	'use strict';

	var app = angular.module('app.enseignants', []);

	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; //clear original array
		this.push.apply(this, array); //push all elements except the one we want to delete
	}
  
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	}
  
	app.factory('enseignantsFactory', ['$http',function($http){
		var list =
//			{
//				"noEnseignant" : "1",
//				"nom" : "SALIOU",
//				"prenom" : "Philippe",
//				"type" : "MCF",
//				"sexe" : "H",
//				"adresse" : "6 rue de l'Argoat",
//				"codePostal" : "29860",
//				"ville" : "LE DRENNEC",
//				"pays" : "France",
//				"mobile" : "06.29.24.01.00",
//				"telephone" : "02.98.01.69.74",
//				"emailPerso" : "philippe.saliou@univ-brest.fr",
//				"emailUbo" : "philippe.saliou@gmail.com"
//			};
			function() {
				return  $http.get('http://localhost:8090/enseignant/findallenseignant') 
			};
		
		return {
			// renvoi la liste de tous les enseignants
			all:list,
			// renvoi l'enseignant avec le no_enseignant demandé
			get: function(noEnseignant) {
				//return  $http.get('http://localhost:8090/enseignant/findEnseignantByNo/1');
				console.log("TODO : get enseignant",noEnseignant);
				return  $http.get('http://localhost:8090/enseignant/findenseignant/'+noEnseignant);
			},
			
			set: function(enseignant) {
				var idx = enseignant.noEnseignant;
				// si modification d'un enseignant existant
				if(idx){
					console.log("TODO : update enseignant",idx);
					var newEnseignant = {
						"noEnseignant" : idx,
						"nom" : enseignant.nom,
						"prenom" : enseignant.prenom,
						"type" : enseignant.type,
						"sexe" : enseignant.sexe,
						"adresse" : enseignant.adresse,
						"codePostal" : enseignant.codePostal,
						"ville" : enseignant.ville,
						"pays" : enseignant.pays,
						"mobile" : enseignant.mobile,
						"telephone" : enseignant.telephone,
						"emailPerso" : enseignant.emailPerso,
						"emailUbo" : enseignant.emailUbo,
					};      	  
					$http.post('http://localhost:8090/enseignant/updateenseignant',newEnseignant);
				} 
				else { // si ajout d'un nouvel enseignant
					// TODO ajouter un enseignant à la liste
//					var nextEnseignant = "";
//					$http.get('http://localhost:8090/enseignant/getNextEnseignant').success(data)(function(data){
//						nextEnseignant = data;
//					})
//					.error(function(data,statut){
//						console.log("impossible d'acceder à la base de données");
//					});
//					var newEnseignant = {
//						"noEnseignant" : "8",
//						//"noEnseignant" : nextEnseignant,
//						"nom" : enseignant.nom,
//						"prenom" : enseignant.prenom,
//						"type" : enseignant.type,
//						"sexe" : enseignant.sexe,
//						"adresse" : enseignant.adresse,
//						"codePostal" : enseignant.codePostal,
//						"ville" : enseignant.ville,
//						"pays" : enseignant.pays,
//						"mobile" : enseignant.mobile,
//						"telephone" : enseignant.telephone,
//						"emailPerso" : enseignant.emailPerso,
//						"emailUbo" : enseignant.emailUbo,
//					};      	  
//					$http.post('http://localhost:8090/enseignant/addEnseignant',newEnseignant);
				}
			},
			
			delete: function(noEnseignant) { 
//				// TODO Supprimer 
//				console.log("TODO : supprimer enseignant",noEnseignant);
//				return  $http.get('http://localhost:8090/enseignant/deleteEnseignant/'+noEnseignant)
			},

			getPromotion : function(noEnseignant){
				var url = "http://localhost:8090/enseignant/getpromotionenseignant/"+noEnseignant;
				return $http.get(url);
			},
  
			getUE : function(noEnseignant){
				var url = "http://localhost:8090/enseignant/getuebyenseignant/"+noEnseignant;
				return $http.get(url);
			}
		};
	}]);

  

	app.controller('EnseignantsController', 
    ['$scope', '$filter','$location', 'enseignantsFactory',
    function($scope, $filter, $location, enseignantsFactory){
//    	$scope.enseignants = enseignantsFactory.all;
//		console.log("Liste des enseignants",enseignantsFactory.all);
//
    	var init;
    	enseignantsFactory.all()
			.success(function(data) {
				$scope.enseignants = data;
				$scope.searchKeywords = '';
				$scope.filteredEnseignant = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageEnseignant = $scope.filteredEnseignant.slice(start, end);
				};
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredEnseignant = $filter('filter')($scope.enseignants, $scope.searchKeywords);
					return $scope.onFilterChange();
				};
				$scope.order = function(rowName) {
					if ($scope.row === rowName) {
						return;
					}
					$scope.row = rowName;
					$scope.filteredEnseignant = $filter('orderBy')($scope.enseignants, rowName);
					return $scope.onOrderChange();
				};
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageEnseignant = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();
			})
			.error(function(data) {
				$scope.error = 'unable to get the poneys';
			});
    	    			
		$scope.ajoutEnseignant = function(){
//			$location.path('/admin/enseignant/nouveau'); 
		}
  
		// affiche les détail d'un enseignant
		// TODO Lien vers la page permettant d'éditer un enseignant /admin/enseignant/ + no_enseignant
		$scope.edit = function (noEnseignant){
			$location.path("/admin/enseignant/"+ noEnseignant);
		}

		// supprime un enseignant
		$scope.supprime = function(noEnseignant){ 
			// TODO Suppression d'un enseignant de la liste
//			var promise= enseignantsFactory.delete(noEnseignant);
//			promise.success(function(data,statut){
//				//$scope.enseignant.promotions = data ;
//				$scope.currentPageEnseignant.removeValue("noEnseignant",noEnseignant);
//			})
//			.error(function(data,statut){
//				console.log("impossible de supprimer l'enseignant choisi");
//			});
		}
    }]);

	app.controller('EnsDetailsController', 
	['$scope', '$routeParams', '$location', 'enseignantsFactory',
	function($scope, $routeParams, $location, enseignantsFactory){      
		$scope.edit= false;    
		// si creation d'un nouvel enseignant
		if($routeParams.id == "nouveau"){
			$scope.enseignant= { };
			$scope.edit= true;    
		} 
		else { // sinon on edite un enseignant existant
			var promise = enseignantsFactory.get($routeParams.id);
			promise.success(function(data){
				$scope.enseignant = data ;
				var promise= enseignantsFactory.getPromotion($routeParams.id);
				promise.success(function(data,statut){
					$scope.enseignant.promotions = data ;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi");
				});
				var promise= enseignantsFactory.getUE($routeParams.id);
					promise.success(function(data,statut){
						$scope.enseignant.ue = data ;
					})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi");
				});
			})
			.error(function(data){
				console.log("impossible de recuperer les details de l'enseignant choisi");
			});
		}
		
		$scope.edition = function(){
			$scope.edit = true;
		}
  
		// valide le formulaire d'édition d'un enseignant
		$scope.submit = function(){    	 
			enseignantsFactory.set($scope.enseignant);        
			$scope.edit = false;        
		}
		
		// annule l'édition
		$scope.cancel = function(){
			// si ajout d'un nouvel enseignant => retour à la liste des enseignants
			if(!$scope.enseignant.noEnseignant){
				$location.path('/admin/enseignants');
			} 
			else {
				var e = enseignantsFactory.get($routeParams.id);
				$scope.enseignant = JSON.parse(JSON.stringify(e));
				$scope.edit = false;
			}
		}      
    }]);
	
}).call(this);
