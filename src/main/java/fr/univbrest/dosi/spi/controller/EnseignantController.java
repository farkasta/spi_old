package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.User;
import fr.univbrest.dosi.spi.service.EnseignantService;
import fr.univbrest.dosi.spi.service.PromotionService;
import fr.univbrest.dosi.spi.service.UniteEnseignementService;

/**
 *Classe controleur de la partie gestion des enseignants
 * @author DOSI
 *
 */

@RestController
public class EnseignantController {
	/**
	 *Injection de enseignantService 
	 */
	@Autowired
	private EnseignantService enseignantService;

	/**
	 *Injection de promotionService 
	 */
	@Autowired
	private PromotionService promotionService;

	/**
	 *Injection de uniteEnseignementService 
	 */
	@Autowired
	private UniteEnseignementService uniteEnseignementService;

	/*
	 * private enum TypeDroit { CREATE, DELETE, MODIFY, SELECT, }
	 *
	 * /** Service de gestion des enseignants
	 */
	
	// private Map<TypeDroit, List<Role>> mapDroits = new HashMap<EnseignantController.TypeDroit, List<Role>>();

	/**
	 * User
	 */
//	@Autowired
//	User user;

	/*
	 * public EnseignantController() { this.configure(); }
	 * 
	 * private void checkDroits(final TypeDroit typeDroit) { if (!this.mapDroits.get(typeDroit).contains(this.user.getRoles())) { throw new SPIException(SpiExceptionCode.NOT_ENOUGH_RIGHT,
	 * "L'utilisateur n'a pas les droits suffisants pour effectuer l'action demandée"); } }
	 * 
	 * private void configure() { this.mapDroits.put(TypeDroit.CREATE, Arrays.asList(Role.ADMIN, Role.PROF)); this.mapDroits.put(TypeDroit.SELECT, Arrays.asList(Role.ETUDIANT, Role.ADMIN, Role.PROF));
	 * this.mapDroits.put(TypeDroit.MODIFY, Arrays.asList(Role.ADMIN, Role.PROF)); this.mapDroits.put(TypeDroit.DELETE, Arrays.asList(Role.ADMIN)); }
	 */

	/**
	 * Methode d'ajout d'un enseignant
	 * @param enseignant
	 *            l'entité de l'enseignant à ajouter
	 * @return le message d'ajout
	 */
	// @RequestMapping(value="/ajouterEnseignant" , headers="Accept=application/json", method=RequestMethod.POST)
	@RequestMapping(value = "/enseignant/addenseignant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public String addEnseignant(@RequestBody Enseignant enseignant) {
		// this.checkDroits(TypeDroit.CREATE);
		enseignantService.addEnseignant(enseignant);
		return "l'enseignant " + enseignant.getNom() + " " + enseignant.getPrenom() + " est ajouter";
	}

	/**
	 * Methode de suppression d'un enseignant
	 * @param noEnseignant
	 *            l'id de l'enseignant à supprimer
	 */
	@RequestMapping(value = "/enseignant/deleteenseignant/{noenseignant}")
	public void deleteEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		// this.checkDroits(TypeDroit.DELETE);
		enseignantService.deleteEnseignant(noEnseignant);
	}

	/**
	 * Methode de recuperation de tous les enseignants
	 * @return liste des enseignant
	 */
	@RequestMapping("/enseignant/findallenseignant")
	public Iterable<Enseignant> findAllEnseignant() {
		// Iterable<Enseignant> enseignants = enseignantService.listens();
		/*
		 * for(Enseignant ens : enseignants){ System.out.println("OK traitement "+ ens.getNom()); }
		 */
		// this.checkDroits(TypeDroit.SELECT);
		return enseignantService.findAllEnseignant();
	}

	/**
	 * Methode de test de l'existance d'un enseignant
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return resultat du test
	 */
	@RequestMapping(value = "/existenseignant/{noenseignant}")
	public Boolean existEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		return enseignantService.existEnseignant(noEnseignant);
	}

	/**
	 * Methode de recherche d'un enseignant
	 * @param id
	 *            l'id de l'enseignant à rechercher
	 * @return l'enseignant
	 */
	@RequestMapping(value = "/enseignant/findenseignant/{noenseignant}")
	public Enseignant findEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		// this.checkDroits(TypeDroit.SELECT);
		return enseignantService.findEnseignant(noEnseignant);
	}

	/**
	 * Methode de recherche d'un/des enseignant(s) par son nom
	 * @param nom
	 *            de l'enseignant
	 * @return la liste des enseignants ayant le nom recherché
	 */
	// @RequestMapping(value ="/getens/{id}")
	@RequestMapping(value = "/enseignant/findenseignantsbynom/{nom}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Enseignant> findEnseignantByNom(@PathVariable(value = "nom") String nom) {
		// this.checkDroits(TypeDroit.SELECT);
		return enseignantService.findEnseignantByNom(nom);
	}

	/**
	 *	Getter du service
	 * @return getter
	 */
	public EnseignantService getEnseignantService() {
		return enseignantService;
	}

	/**
	 *Methode de recuperation des promotion qui ont pour responsable l'enseignant en parametre
	 * @param noEnseignant
	 *            l'id de l'enseignant 
	 * @return liste des promotions
	 */
	@RequestMapping(value = "/enseignant/findpromotionsbyenseignant/{noenseignant}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Promotion> findPromotionsByEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		return promotionService.getPromotionByEnseignant(noEnseignant);
	}

	
	public PromotionService getPromotionService() {
		return promotionService;
	}

	/**
	 *Methode de recuperation des Unités d'enseignement qui ont pour responsable l'enseignant en parametre
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return liste des unite enseignant
	 */
	@RequestMapping("/enseignant/finduesbyenseignant/{noenseignant}")
	public List<UniteEnseignement> findUEsByEnseignant(@PathVariable("noenseignant") Integer noEnseignant) {
		return uniteEnseignementService.getUEsByEnseignant(noEnseignant);
	}

	public UniteEnseignementService getUniteEnseignementService() {
		return uniteEnseignementService;
	}

	public void setEnseignantService(final EnseignantService enseignantService) {
		this.enseignantService = enseignantService;
	}

	public void setPromotionService(final PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setUniteEnseignementService(final UniteEnseignementService uniteEnseignementService) {
		this.uniteEnseignementService = uniteEnseignementService;
	}

	/**
	 *Methode de modic=fication d'un enseignant
	 * @param enseignant
	 *            objet
	 * @return message de modification
	 */
	// @RequestMapping(value="/ajouterEnseignant" , headers="Accept=application/json", method=RequestMethod.POST)
	@RequestMapping(value = "/enseignant/updateenseignant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public String updateEnseignant(@RequestBody Enseignant enseignant) {
		// this.checkDroits(TypeDroit.MODIFY);
		enseignantService.updateEnseignant(enseignant);
		return "l'enseignant " + enseignant.getNom() + " " + enseignant.getPrenom() + " est modifier";
	}
}
