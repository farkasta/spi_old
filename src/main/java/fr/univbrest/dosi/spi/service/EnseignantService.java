package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.dao.EnseignantRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * Classe Service de l'enseignant
 * @author DOSI
 *
 */
@Service
public class EnseignantService {
	/**
	 *Injection du repository enseignant
	 */
	@Autowired
	private EnseignantRepository enseignantRepository;

	/**
	 * Methode d'ajout d'un enseignant
	 * @param enseignant
	 *            l'entité à ajouter
	 * @return l'enseignant ajouté
	 */
	public Enseignant addEnseignant(Enseignant enseignant) {
		if (enseignantRepository.exists(enseignant.getNoEnseignant())) {
			throw new SPIException("l'enseignant que vous souhaitez ajouter exsite déja ");
		}
		return enseignantRepository.save(enseignant);
	}

	/**
	 * Meythode de suppression d'un enseignant
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 */
	public void deleteEnseignant(Integer noEnseignant) {
		// enseignantRepository.delete(noEnseignant);
		// return enseignantRepository.findAll();
		if (enseignantRepository.exists(noEnseignant)) {
			enseignantRepository.delete(noEnseignant);
		} else {
			throw new SPIException("Cant delete Enseignant");
		}
		/*
		 * try { enseignantRepository.delete(noEnseignant); } catch (EmptyResultDataAccessException e1) { throw new SPIException("Il y a aucun enseignant avec ce numero", e1); } catch (Exception e) {
		 * throw new SPIException("Cant delete Enseignant", e); }
		 */
	}

	/**
	 * Methode de test de l'existance d'un enseignant
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return resultat du test
	 */
	public Boolean existEnseignant(Integer noEnseignant) {
		final Boolean exist = enseignantRepository.exists(noEnseignant);
		if (exist) {
			return exist;
		} else {
			throw new SPIException("Il y a aucun enseignant avec ce numero");
		}
	}

	/*
	 * public Enseignant addEnseignant(Enseignant enseignant) { Boolean exist = enseignantRepository.exists(enseignant .getNoEnseignant()); if (exist) { throw new SPIException(
	 * "l'enseignant que vous souhaitez ajouter exsite déja ");
	 * 
	 * } else { return enseignantRepository.save(enseignant); } }
	 */
	/**
	 * Methode de recherche d'un enseignant
	 * @param id
	 *            l'id de l'enseignant à rechercher
	 * @return l'enseignant
	 */
	public Enseignant findEnseignant(Integer noEnseignant) {
		return enseignantRepository.findOne(noEnseignant);
	}

	/**
	 * Methode de recherche d'un/des enseignant(s) par son nom
	 * @param nom
	 *            de l'enseignant
	 * @return la liste des enseignants ayant le nom recherché
	 */
	public List<Enseignant> findEnseignantByNom(String nom) {
		return enseignantRepository.findByNom(nom);
	}

	/**
	 *	Getter du repository
	 * @return getter
	 */
	public final EnseignantRepository getEnseignantRepository() {
		return enseignantRepository;
	}

	/**
	 * Methode de recuperation de tous les enseignants
	 * @return liste des enseignant
	 */
	public Iterable<Enseignant> findAllEnseignant() {
		final Iterable<Enseignant> enseignants = enseignantRepository.findAll();
		return enseignants;
	}

	/**
	 *	Setter du repository
	 * @param enseignantRepository
	 *            setter
	 */
	public final void setEnseignantRepository(final EnseignantRepository enseignantRepository) {
		this.enseignantRepository = enseignantRepository;
	}

	/**
	 * Methode de modification d'un enseignant
	 * @param enseignant
	 *            l'entité enseignant à modifié
	 * @return l'enseignant modifié
	 */
	public Enseignant updateEnseignant(Enseignant enseignant) {
		if (enseignantRepository.exists(enseignant.getNoEnseignant())) {
			return enseignantRepository.save(enseignant);
		} else {
			throw new SPIException("l'enseignant que vous souhaitez modifier n'exsite pas ");
		}
	}

}
